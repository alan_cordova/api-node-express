var express = require('express');
var http = require("http");
var https = require("https");
var querystring = require('querystring');
var request = require('request');
var rp = require('request-promise');
var cheerio = require('cheerio');
var defer = require('promise-defer');
var date = require('date-and-time');

var app = express();

var environment = 'prod';

var domain = (environment == 'prod') ? 'sbpvr.com' : 'sbpvr.local';
var apiDomain = (environment == 'prod') ? 'api.sbpvr.com' : 'sb-api-laravel.local'


function makeRequest(method, req, callback){
    method.get(req, (res) => {
        var data = "";

        res.on("data", (chunk) => {
            data += chunk;
        });

        res.on("end", () => {
            if(typeof callback == 'function'){
                callback(data);
            }
        });
        res.on('error', () => { 
            console.log('error');
        });
    });
}

/* PROPERTIES SB */
function runSB(){
    var path = (environment == 'prod') ? '/wp-content/themes/sbpvr2/includes/ajax/get_properties.php' : '/dummy-properties.json';
    var method = (environment == 'prod') ? https : http;
    
    var req = { host: domain, path: path };
    makeRequest(method, req, (res) => {
        try { 
            saveProperties(JSON.parse(res));
        } catch (error) {
            sendAlert('Error al obtener propiedades JSON - SB');
        }
    });
}

/* END PROPIEDADES SB */



/* PROPERTIES MLS */
//Init functio to request and save all properties to API
function runMLS(){
    var req = { host: 'sbpvr.com', path: '/mls/search' };
    makeRequest(https, req, (res) => {
        try {  
            var total_pages = getTotalPagesMLS(res);
            getPropertiesMLS( total_pages, 1 ).then( (allProperties) => {
                //aqui todas las propiedades en un array con sus datos...
                console.log('completed');
            });
        } catch (error) {
            sendAlert('Error al obtener propiedades MLS' + error);
        }
    });
}

//gets the total pages to request from MLS
function getTotalPagesMLS(html){
    var $ = cheerio.load(html);
    var total_pages = 0;
    var paginationContainer = cheerio.load($('.flexmls_connect__sr_pagination').html());
    paginationContainer('a').each(function(i, elem){
        if(i == paginationContainer('a').length - 1){
           total_pages = parseInt($(elem).text());
        }
    });
    return total_pages;
}

//recursive function to get all properties from a current page of MLS in SBPVR and save in API
function getPropertiesMLS(total_pages, current){
    var def = defer();

    var options = {
        method: 'GET',
        uri: 'https://sbpvr.com/mls/search?pg=' + current + '&OrderBy=-ListPrice',
    };

    rp(options).then( (res) => {

        if(current <= (total_pages)){

            //request of a batch of N properties with their attributes
            getPropertiesFromPageMLS(res).then( ( propertiesBatch ) => {
                var next = current + 1;

                //save properties batch to API
                console.log('saving lot ' + current);
                savePropertiesBatch(propertiesBatch);
                console.log('SAVED lot ' + current);

                //request the next MLS page from SBPVR
                getPropertiesMLS(total_pages, next).then( (allProperties) => {
                    var merge = allProperties.concat(propertiesBatch);
                    def.resolve(merge);
                } );

            });
        }else{
            def.resolve([]);
        }

    });
    
    return def.promise;
}

//returns a list of N properties with their attributes
function getPropertiesFromPageMLS(html){
    var $ = cheerio.load(html);

    var propertiesToRequest = [];
    var propertyHTMLSections = [];

    //prepares the list of N properties (urls) to request
    $('.flexmls_connect__sr_result').each(function(i, elem){
        $(elem).find('.flexmls_connect__sr_address').each(function(i, elem){
            var href = $(elem).find('a').attr('href');
            propertiesToRequest.push(href);
        });


        propertyHTMLSections.push($(elem));
    });

    return getPropertiesFromPageRequestMLS(propertiesToRequest, propertyHTMLSections, 0);
}

//make the request of N properties recursively
//return properties with their full attributes
function getPropertiesFromPageRequestMLS(properties, propertyHTMLSections, current){
    var def = defer();

    if(properties.length == 0){
        def.resolve([]);
    }else{
        var options = {
            method: 'GET',
            uri: properties[current]
        };

        rp(options).then( (propertyPageHTML) => {

            //get attributes from a single property using the HTML code from that single property page
            var propertyData = getPropertyData(propertyPageHTML, propertyHTMLSections[current]);

            var next = current + 1;
            if(next < properties.length && typeof properties[next] != "undefined" ){
                var p = getPropertiesFromPageRequestMLS(properties, propertyHTMLSections, next);
                p.then( ( promiseResult ) => {
                    promiseResult.push(propertyData);
                    def.resolve( promiseResult );
                });
            }else{
                def.resolve( [propertyData] );
            }
        });

    }


    return def.promise;
}

/** propertyPage = the full html page from MLS property **/
/** propertySection = the section html from MLS page (from paginator) **/
function getPropertyData(propertyPage, propertyCheerioSection){
    var $ = cheerio.load(propertyPage);

    var permalink =  propertyCheerioSection.find('.flexmls_connect__sr_address a').attr('href');
    var title =  propertyCheerioSection.find('.flexmls_connect__sr_address a').text();
    var img =  propertyCheerioSection.find('.flexmls_connect__sr_main_photo').attr('src');
    //var img = $('.flexmls_connect__photos .flexmls_connect__photo_container img').attr('src');
    var getIdPermalink = permalink.substr(permalink.indexOf('mls_'));
    var propertyId = getIdPermalink.substr(permalink, getIdPermalink.indexOf('?'));

    var lat = $('#flexmls_connect__map_canvas').attr('latitude');
    var lng = $('#flexmls_connect__map_canvas').attr('longitude');
    var price = $('.flexmls_connect__ld_price').html();
    var bathsAndBeds = getBathsBeds($);

    var parking = $('#flexmls_connect__detail_group').find('.flexmls_connect__ld_property_detail:contains("Parking:")').text();

    var features = getFeatures($);
    var description = (getDescription($)) ? getDescription($) : '';
    var formatPrice = (price) ? Number(price.replace(/[^0-9\.-]+/g,"")) : null;
    var type = propertyCheerioSection.find('.flexmls_connect__sr_listing_facts .flexmls_connect__zebra .flexmls_connect__field_label:contains("Property Type: ")').siblings('.flexmls_connect__field_value').text().toLowerCase().replace(/ /g, "-");
    var location = propertyCheerioSection.find('.flexmls_connect__sr_listing_facts .flexmls_connect__zebra .flexmls_connect__field_label:contains("Area:")').siblings('.flexmls_connect__field_value').text().toLowerCase().replace(/ /g, "-");
    var getLastUpdate = propertyCheerioSection.find('.flexmls_connect__sr_listing_facts .flexmls_connect__zebra .flexmls_connect__field_label:contains("Last Updated:")').siblings('.flexmls_connect__field_value').text().replace(/ /g, "").replace(/-/g, " ");
    var convertToDate = date.parse(getLastUpdate, 'MMMM D YYYY'); 
    var day = convertToDate.getDate();
    var month = ("0" + (convertToDate.getMonth() + 1)).slice(-2);
    var year = convertToDate.getFullYear();
    var formatDate = year + '-' + month + '-' + day;

    var property = {
        'is_mls' : 1,
        'permalink': (permalink) ? permalink : null ,
        'title': (title) ? title : null,
        'description': (description) ? description : null,
        'images': (img) ? img : null,
        'lat': lat,
        'lng': lng,
        'price': formatPrice,
        'price_type': '',
        'lang': 'en',
        'property_id': (propertyId) ? propertyId : '',
        'last_update': formatDate,
        'baths' : (bathsAndBeds.baths) ? bathsAndBeds.baths : null,
        'beds' : (bathsAndBeds.beds) ? bathsAndBeds.beds : null,
        'parking': (parking) ? 1 : null,
        'status_id': 1,
        'location': (location) ? location : 'none',
        'features': (features) ? features : [],
        "propertyType": (type) ? type : 'other',
    };

    return property;
}

function getBathsBeds($){
    return $('.flexmls_connect__sr_address').contents().filter(function() {
        return this.nodeType == 3 && $(this).text().indexOf('beds')>-1;
    }).text().split('|').reduce(function(a,c){
        if(c.indexOf('beds')>-1){
            a.beds = /\d+/.exec(c)[0]
        }else if(c.indexOf('baths')>-1){
            a.baths = /\d+/.exec(c)[0]
        }
        return a
    },{});
}

function getFeatures($){
    var features = [];
    var str_features = '';
    $('.flexmls_connect__ld_detail_table .flexmls_connect__detail_header:contains("Property Features")').each(function () {
        $(this).siblings().find('.flexmls_connect__ld_property_detail_row').each(function () {
            $(this).find("b").remove();
            str_features = $(this).find('.flexmls_connect__ld_property_detail').html();
            str_features = str_features.trim().toLowerCase();//.replace(/ /g, "-");
            features.push(str_features);
        });
    });

    return features;
}

function getDescription($){
    return $('.flexmls_connect__sr_detail:not(b)').contents().filter(function() {
        return this.nodeType == 3;
    }).text();
}

function saveProperties(properties){
    var batch = [];
    for(var i=0; i < properties.length; i++){
        sent = false;
        batch.push(properties[i]);

        if(batch.length == 5){
            savePropertiesBatch(batch);
            batch = [];
            sent = true;
        }
    }

    if(!sent){
        savePropertiesBatch(batch);
    }
    
}

function savePropertiesBatch(properties){
    console.log('saving batch');
    var url = 'https://' + apiDomain + '/properties/storeProperties';
    request.post(url , { form: {properties: properties} }, (err, res, body) => {
        console.log('batch save success');
    });
}


/* END PROPIEDADES MLS */


function init(){
    runSB();
    runMLS();
}


init();


function sendAlert(msg){
    if (environment == 'prod') {
        var url = 'https://sbpvr.com/wp-content/themes/sbpvr2/includes/ajax/msgerror/sendMsgErrorApi.php';
        request.post(url , { form: {msg: msg} });
    }else{
        console.log(msg);
    }
}



