$(function() {
    var _token = $('input[name="_token"]').val();

    function savePropertiesSB(){
        $.get( "https://sbpvr.com/wp-content/themes/sbpvr2/includes/ajax/get_properties.php", function( data ) {
            $.post('http://apiproperties.local/properties/storeSB', {_token : _token, propertiesString: data});
        });
    }

    function savePropertiesMLS(total_pages){
        var number_page = 0;
        var loaded = 0;
        var url;
        var callSameTime = true;
        var callSameTimeDetail = true;
        while (number_page <= total_pages) {
            url = 'https://sbpvr.com/mls/search?pg='+number_page+'&OrderBy=-ListPrice';
            if (callSameTime) {
                $.post(url, function(data) {
                    callSameTime = false;
                    $(data).find('div.flexmls_connect__sr_result').each(function(){
                        var link_property = $(this).find('.flexmls_connect__sr_address a').attr('href');
                        var property = $(this);
                        if (callSameTimeDetail) {
                            $.post(link_property, function (res) {
                                callSameTimeDetail = false;
                            }).done(function(res){
                                sendData(propertyData(res, property));
                                callSameTimeDetail = true;
                            });
                        }
                    });
                    callSameTime = true;
                });
                loaded++;
            }
            number_page ++;
        }
    }

    function init(){
        var url = 'https://sbpvr.com/mls/search';
        $.post(url, function(data) {
            var total_pages = $(data).find('.flexmls_connect__sr_pagination a').last().html();
            total_pages = parseInt(total_pages);
            if(total_pages > 0){
                savePropertiesMLS(total_pages);
            }
        });
    }

    function sendData(data){
        $.ajax({
            type: "POST",
            url: './storeMLS',
            data: {
                _token : _token,
                propertiesString: data
            }
        })
    }

    function propertyData(res, property){
        var results_page = [];
        var permalink = verifyData(property.find('.flexmls_connect__sr_address a').attr('href'));
        var title = verifyData(property.find('.flexmls_connect__sr_address a').html());
        var description = $(res).find('.flexmls_connect__sr_detail:not(b)').contents().filter(function() {
            return this.nodeType == 3;
        }).text();
        var img = verifyData(property.find('.flexmls_connect__sr_main_photo').attr('src'));
        var lat = verifyData($(res).find('#flexmls_connect__map_canvas').attr('latitude'));
        var lng = verifyData($(res).find('#flexmls_connect__map_canvas').attr('longitude'));
        var type = verifyData(property.find('.flexmls_connect__sr_listing_facts .flexmls_connect__zebra').first().find('.flexmls_connect__field_value').html().toLowerCase().replace(/ /g, "-"));
        var location = '';
        $(property.find('.flexmls_connect__sr_listing_facts .flexmls_connect__zebra .flexmls_connect__field_label:contains("Area:")')).each(function () {
            location = $(this).siblings().html().toLowerCase().replace(/ /g, "-");
        });
        var price = verifyData($(res).find('.flexmls_connect__ld_price').html());
        var bathsAndBeds = verifyData($(res).find('.flexmls_connect__sr_address').contents().filter(function() {
            return this.nodeType == 3 && $(this).text().indexOf('beds')>-1;
        }).text().split('|').reduce(function(a,c){
            if(c.indexOf('beds')>-1){
                a.beds = /\d+/.exec(c)[0]
            }else if(c.indexOf('baths')>-1){
                a.baths = /\d+/.exec(c)[0]
            }
            return a
        },{}));
        var features = [];
        var str_features = '';
        $(res).find('.flexmls_connect__ld_detail_table .flexmls_connect__detail_header:contains("Property Features")').each(function () {
            $(this).siblings().find('.flexmls_connect__ld_property_detail_row').each(function () {
                $(this).find("b").remove();
                str_features = $(this).find('.flexmls_connect__ld_property_detail').html();
                str_features = $.trim(str_features).toLowerCase();//.replace(/ /g, "-");
                features.push(str_features);
            });
        });

        var formatPrice = '';

        if (price) {
            formatPrice = Number(price.replace(/[^0-9\.-]+/g,""));
        }else{
            formatPrice = '';
        }

        if (description) {
            description = description;
        }else{
            description = '';
        }

        results_page.push({
            permalink: permalink,
            title: title,
            description: description,
            img: img,
            position: {lat: lat, lng: lng},
            type: type,
            location: location,
            price: formatPrice,
            baths: bathsAndBeds.baths,
            beds: bathsAndBeds.beds,
            features: features
        });

        return results_page;
    }

    function verifyData(item){
        if (item) {
            item = item;
        }else{
            item = '';
        }
        return item;
    }

    //init();
    savePropertiesSB();
});